import React from 'react';
import { cloneDeep, concat, filter, forEach, isEmpty, map, parseInt } from 'lodash';

class Home extends React.Component {

	constructor(props) {
		super(props);
		
		this.state = {
			todos: [],		// Call them what they are
			nextTodoId: 1
		};
		
		// Keep all bindings in one place
		this.onSaveTodo = this.onSaveTodo.bind(this);
		this.onDeleteTodo = this.onDeleteTodo.bind(this); 	
		this.toggleDoneTodo = this.toggleDoneTodo.bind(this);
	}	
	

	onSaveTodo(){
		let name = this.refs.txtTodoName.value;

		/* You don't use this.state.empty anywhere. Don't set it's value, instead just stop executing */
		/* No need for "keep", you don't use it */
		
		// If name value is not empty (eg. "" or null or undefined) add to new state
		if (!isEmpty(name)) {
			this.setState({
				todos: concat(this.state.todos, {
					id: this.state.nextTodoId, 			// Use a unique Id for each todo item (would be even better to generate a UUID)
					name: name, 						// Keep naming consistent
					done: false
				}),
				nextTodoId: this.state.nextTodoId + 1 	// Keep unique
			});

			// Only set input value to empty if validation (ie. if not empty) passes
			this.refs.txtTodoName.value="";
		}
	}
	
	onDeleteTodo(event){ // Use consistent method names. In this case, using on{action}Todo shows the methods are related (readability)
		let todoId = parseInt(event.target.dataset.value);
		
		this.setState({
			todos: filter(this.state.todos, (todo)=>{
				return todo.id !== todoId;
			})
		});	 
	}
	
	toggleDoneTodo(event){ // Again, actionable method name. Use 'toggle' cos you can turn it on AND off with same method
		let todoId = parseInt(event.target.dataset.value);
		let newTodos = cloneDeep(this.state.todos);

		/* 
			!DO NOT MUTATE EXISTING STATE OBJECT!
			Create new object, mutate it, and THEN set state to value of new object
		*/

		// Much easier to set new state to inverse of old state instead of all those if statements
		// Also, notice what would've happened with "... = !..." without spaces (ie. "...!=...", completely different operation)
		forEach(newTodos, (todo)=>{
			if (todo.id === todoId) todo.done = !todo.done;
		});

		this.setState({
			todos: newTodos
		});
	}
	
	render() {
		/* FYI you can do business logic within the render method. Keep it minimal but use it when needed */

		// Keep component definitions separate for easier readability. 
		// In extreme cases you can abstract the following code to its own method
		let todoComponents = map(this.state.todos, (todo, index)=>{ // Call it what it is
			return (
				<li key={index}>
					<p onClick={this.toggleDoneTodo} data-value={todo.id} style={{textDecoration: todo.done ? 'line-through':'none' }}>{todo.name}</p>
					<button onClick={this.toggleDoneTodo} data-value={todo.id}>strike</button>
					<button onClick={this.onDeleteTodo} data-value={todo.id}>remove</button>
				</li>
			);
		});

		return (
			<div className="App">
				<input placeholder="I have to ..." ref="txtTodoName" />
				<button type="button" onClick={this.onSaveTodo}>Add Todo</button>
				<ul>
				  	{ todoComponents } 							 
			    </ul>					  
			</div>
		);
    }
} 

export default  Home;