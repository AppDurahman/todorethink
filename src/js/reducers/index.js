import { createStore, combineReducers } from 'redux';

import counter from './counter';

export const store = 
createStore(combineReducers({
	counter
}));